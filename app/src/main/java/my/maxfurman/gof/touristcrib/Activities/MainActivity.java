package my.maxfurman.gof.touristcrib.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;


import my.maxfurman.gof.touristcrib.Model.MyPlace;
import my.maxfurman.gof.touristcrib.R;
import my.maxfurman.gof.touristcrib.Utils.DialogActivityNet;
import my.maxfurman.gof.touristcrib.Utils.PlaceSingleton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import my.maxfurman.gof.touristcrib.Utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Spinner mSpinnerPrice;
    private Spinner mSpinnerDistrict;
    private Spinner mSpinnerType;
    private Button mButton;

    private int mSpinnerPriceIndex;
    private int mSpinnerDistrictIndex;
    private int mSpinnerTypeIndex;
    private ArrayList<Integer> mIntegerArrayList;
    List<MyPlace> localList;

    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;


    private boolean mLocationPermissionGranted = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);




        checkLocationPermission();



        setBar();
        mIntegerArrayList = new ArrayList<>();
        mSpinnerPriceIndex = 0;
        mSpinnerDistrictIndex = 0;
        mSpinnerTypeIndex = 0;
        initSpinner(mSpinnerPrice, R.id.spinner1, R.array.spinner_array1);
        initSpinner(mSpinnerDistrict, R.id.spinner2, R.array.spinner_array2);
        initSpinner(mSpinnerType, R.id.spinner3, R.array.spinner_array3);

        mButton = (Button) findViewById(R.id.button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //if(!checkLocationPermission()) ;

                if(!isNetworkConnected()){
//                    PopupNetSingleton popupNetSingleton = PopupNetSingleton.get(getBaseContext());
//                    popupNetSingleton.openPopupWindow();
                    showNetworkDialog();
                    return;
                }

                //getFirebaseData();

                checkLocationPermission();

                if(mIntegerArrayList.size() > 0) mIntegerArrayList.clear();
                mIntegerArrayList.add(mSpinnerPriceIndex);
                mIntegerArrayList.add(mSpinnerDistrictIndex);
                mIntegerArrayList.add(mSpinnerTypeIndex);


                //Intent i = new Intent(MainActivity.this,ListActivity.class);
                Intent i = new Intent(MainActivity.this, PlaceListActivity.class);
                i.putIntegerArrayListExtra(Constants.filterResult,mIntegerArrayList);
                startActivity(i);
            }
        });

//        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = firebaseDatabase.getReference();
//
//        myRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                mButton.setText(dataSnapshot.getValue().toString());
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });



    }

    public void setBar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Hide support action bar and disable drawer layout
        getSupportActionBar().hide();
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    protected void initSpinner(Spinner mSpinner, int spinId, int array){
        mSpinner = (Spinner) findViewById(spinId);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                array, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.spinner_item);
        // Apply the adapter to the spinner
        mSpinner.setAdapter(adapter);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (parent.getId()){
                    case R.id.spinner1:
                        mSpinnerPriceIndex = position;
                        break;
                    case R.id.spinner2:
                        mSpinnerDistrictIndex = position;
                        break;
                    case R.id.spinner3:
                        mSpinnerTypeIndex = position;
                        break;
                    default:
                        break;
                }

                //Toast.makeText(getApplicationContext(), parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
    }

    public void getFirebaseData(){


        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference("places");
        mDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String,String> hashMap = new HashMap<String, String>();
                PlaceSingleton placeSingleton = PlaceSingleton.get(getApplicationContext());
                localList = placeSingleton.getPlaces();


                for(DataSnapshot postSnapshot: dataSnapshot.getChildren()){
                    MyPlace myPlace = postSnapshot.getValue(MyPlace.class);
                    myPlace.setUuid(postSnapshot.getKey());
                    hashMap.put(myPlace.getUuid(),myPlace.getPlaceId());

                    boolean alreadyIs = false;
                    for(MyPlace place : localList){
                        if(place.getPlaceId().equals(myPlace.getPlaceId())){
                            alreadyIs = true;
                            break;
                        }
                    }

                    if(!alreadyIs){
                        placeSingleton.addPlace(myPlace);
                    }

                    //mDatabaseReference.removeEventListener(this);

                        //localList.add(myPlace);
                }

//                uploadDatabase(hashMap);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("DATA", "The read failed: " + databaseError.getMessage());

            }
        });
    }

    public void checkLocationPermission(){
          /*
     * Request location permission, so that we can get the location of the
     * device. The result of the permission request is handled by a callback,
     * onRequestPermissionsResult.
     */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public void showNetworkDialog(){
        DialogActivityNet dialogNet = new DialogActivityNet();
        dialogNet.show(getFragmentManager(),"dialogNet");
    }



}
