package my.maxfurman.gof.touristcrib.Activities;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import my.maxfurman.gof.touristcrib.Model.MyPlace;
import my.maxfurman.gof.touristcrib.R;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.PlacePhotoResult;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import my.maxfurman.gof.touristcrib.Utils.Constants;


public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnInfoWindowClickListener, ActivityCompat.OnRequestPermissionsResultCallback {


    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference refId;


    private GoogleApiClient mGoogleApiClient;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;

    private MyPlace myPlace;
    private String placeId;
    private String placeName;
    private String placeRating;
    private String filters;

    private String placeAddress;
    private String placeCity;


    private LatLng placeLatLng;

    private Bitmap bitmap;



    boolean mLocationPermissionGranted = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        myPlace = getIntent().getParcelableExtra(Constants.placeForMap);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                                .findFragmentById(R.id.map);
                        mapFragment.getMapAsync(MapActivity.this);
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .build();


        //Bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        //getSupportActionBar().hide();


    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (placeId == null)
            placeId = myPlace.getPlaceId();

        mMap = googleMap;

        mMap.setOnMyLocationButtonClickListener(this);

        mMap.setOnInfoWindowClickListener(this);

        // Do other setup activities here too, as described elsewhere in this tutorial.

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();

        // Do other setup activities here too, as described elsewhere in this tutorial.
        setupWindowInfo();

        //getPlaceById();




    }



    public void getPlaceById() {
        if(placeId == null)
            placeId = myPlace.getPlaceId();

        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
        placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
            @Override
            public void onResult(@NonNull PlaceBuffer places) {
                if (places.getStatus().isSuccess() && places.getCount() > 0) {
                    Place place = places.get(0);
                    placeLatLng = place.getLatLng();
                    placeName = place.getName().toString();

                    MarkerOptions options = new MarkerOptions()
                            .position(placeLatLng);

                    Marker marker = mMap.addMarker(options);

                    marker.showInfoWindow();


                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(placeLatLng)
                            .zoom(15)
                            .build();
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
                    mMap.moveCamera(cameraUpdate);
                }
                places.release();
            }
        });
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

//    /*
//     * Request location permission, so that we can get the location of the
//     * device. The result of the permission request is handled by a callback,
//     * onRequestPermissionsResult.
//     */
//        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
//                android.Manifest.permission.ACCESS_FINE_LOCATION)
//                == PackageManager.PERMISSION_GRANTED) {
//            mLocationPermissionGranted = true;
//        } else {
//            ActivityCompat.requestPermissions(this,
//                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
//                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
//        }
//
//        if (mLocationPermissionGranted) {
//            mMap.setMyLocationEnabled(true);
//            mMap.getUiSettings().setMyLocationButtonEnabled(true);
//        } else {
//            mMap.setMyLocationEnabled(false);
//            mMap.getUiSettings().setMyLocationButtonEnabled(false);
//            //mLastKnownLocation = null;
//        }
    }

    private void getDeviceLocation() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
        // A step later in the tutorial adds the code to get the device location.
    }

//    @Override
//    public void onRequestPermissionsResult(int requestCode,
//                                           @NonNull String permissions[],
//                                           @NonNull int[] grantResults) {
//        mLocationPermissionGranted = false;
//        switch (requestCode) {
//            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    mLocationPermissionGranted = true;
//                }
//            }
//        }
//        updateLocationUI();
//    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
//        if (placeId != null) {
//            outState.putString(mapResult, placeId);
//        }

        if(myPlace != null) {
            outState.putParcelable(Constants.mapResult,myPlace);
        }
        Log.e("CHECK", "onSaveInstanceState is called");
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.e("CHECK", "onRestoreInstanceState is called");
//        if (savedInstanceState != null)
//            placeId = savedInstanceState.getString(mapResult);
//        Intent i = getIntent();
//        if (i.hasExtra(mapResult)) {
//            placeId = i.getStringExtra(mapResult);
//            i.removeExtra(mapResult);
//        }

        if (savedInstanceState != null)
            myPlace = savedInstanceState.getParcelable(Constants.mapResult);
        Intent i = getIntent();
        if (i.hasExtra(Constants.mapResult)) {
            myPlace = i.getParcelableExtra(Constants.mapResult);
            i.removeExtra(Constants.mapResult);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void setupWindowInfo() {

        Places.GeoDataApi.getPlacePhotos(mGoogleApiClient, placeId)
                .setResultCallback(new ResultCallback<PlacePhotoMetadataResult>() {

                    @Override
                    public void onResult(PlacePhotoMetadataResult photos) {
                        if (!photos.getStatus().isSuccess()) {
                            return;
                        }

                        PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();
                        if (photoMetadataBuffer.getCount() > 0) {
                            // Display the first bitmap in an ImageView in the size of the view
                            photoMetadataBuffer.get(0)
                                    .getScaledPhoto(mGoogleApiClient, 200, 200)
                                    .setResultCallback(mDisplayPhotoResultCallback);
                        }
                        photoMetadataBuffer.release();
                    }

                    private ResultCallback<PlacePhotoResult> mDisplayPhotoResultCallback
                            = new ResultCallback<PlacePhotoResult>() {
                        @Override
                        public void onResult(PlacePhotoResult placePhotoResult) {
                            if (!placePhotoResult.getStatus().isSuccess()) {
                                return;
                            }

                            bitmap = placePhotoResult.getBitmap();

                            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                @Override
                                // Return null here, so that getInfoContents() is called next.
                                public View getInfoWindow(Marker arg0) {
                                    return null;
                                }

                                @Override
                                public View getInfoContents(Marker marker) {

                                    // Inflate the layouts for the info window, title and snippet.
//                ContextThemeWrapper wrapper = new ContextThemeWrapper(getApplicationContext(), R.style.TransparentBackground);
//                LayoutInflater inflater = (LayoutInflater) wrapper.getSystemService(LAYOUT_INFLATER_SERVICE);


                                    LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
                                    final View layout = inflater.inflate(R.layout.custom_infowindow, null);

                                    ImageView imageView = (ImageView) layout.findViewById(R.id.img_list_item);
                                    TextView placeNameTxt = (TextView) layout.findViewById(R.id.place_title);
                                    TextView placeFilterTxt = (TextView) layout.findViewById(R.id.place_filters);
                                    RatingBar ratingBar = (RatingBar) layout.findViewById(R.id.ratingBar_small);

                                    String filters = getResources().getStringArray(R.array.filter_names)[0] +
                                            ": " + getResources().getStringArray(R.array.spinner_array1)[myPlace.getPriceIndex()]
                                            + "\n" + getResources().getStringArray(R.array.filter_names)[1] +
                                            ": " + getResources().getStringArray(R.array.spinner_array2)[myPlace.getDistrictIndex()]
                                            + "\n" + getResources().getStringArray(R.array.filter_names)[2] +
                                            ": " + getResources().getStringArray(R.array.spinner_array3)[myPlace.getTypeIndex()];

                                    placeNameTxt.setText(myPlace.getPlaceName()+"");
                                    placeFilterTxt.setText(filters);
                                    ratingBar.setRating(myPlace.getPlaceRating());

                                    if (bitmap != null) {
                                        imageView.setImageBitmap(bitmap);
                                    }


                                    return layout;
                                }
                            });

                            getPlaceById();

                        }
                    };
                });

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        LatLng latLng = new LatLng(50.439871,30.52417299999999);
        //Create a Uri from an intent string. Use the result to create an Intent.
        //String coordinates = "http://maps.google.com/maps/geo?ll=" + latLng.latitude + "," +latLng.longitude;
//        String coordinates;
//        if (placeLatLng != null) {
////            coordinates = "http://maps.googleapis.com/maps/api/directions/xml?"
////                    + "origin=" + myLatLng.latitude + "," + myLatLng.longitude
////                    + "&destination=" + latLng.latitude + "," + latLng.longitude
////                    + "&sensor=false&units=metric&mode=driving";
//
////            coordinates = "http://maps.google.com/maps?saddr="+placeLatLng.latitude+","+placeLatLng.longitude+
////                    "&daddr="+placeLatLng.latitude+","+placeLatLng.longitude;
//            coordinates = "http://maps.google.com/maps?daddr="+placeLatLng.latitude+","+placeLatLng.longitude;
//        } else {
//            coordinates = "http://maps.google.com/maps/geo?ll=" + placeLatLng.latitude + "," + placeLatLng.longitude;
//        }
        //50.439871,30.52417299999999
        //String coordinates = "geo:50.439871,30.52417299999999?q=50.439871,30.52417299999999("+placeName+")";
        //String coordinates = "geo:" + latLng.latitude + "," +latLng.longitude + "?q="+latLng.latitude + "," +latLng.longitude+"("+placeName+")";
        //String coordinates = "http://maps.google.com/maps?q="+ latLng.latitude  +"," + latLng.longitude +"("+ myPlace.getPlaceAddress() + ")&iwloc=A&hl=es";

        String coordinates = "geo:"+ latLng.latitude  +"," + latLng.longitude +"?q=" + myPlace.getPlaceName() + "," + myPlace.getPlaceAddress();
        //Log.e("coordinates", "" + coordinates);
        //Uri gmmIntentUri = Uri.parse(coordinates);
        Uri gmmIntentUri = Uri.parse(coordinates);
        //Log.e("coordinates", "" + gmmIntentUri);
        //Uri gmmIntentUri =Uri.parse("google.navigation:q=an+" + myPlace.getPlaceAddress() + "+Киев");
        // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        // Make the Intent explicit by setting the Google Maps package
        mapIntent.setPackage("com.google.android.apps.maps");

        // Attempt to start an activity that can handle the Intent
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }
}