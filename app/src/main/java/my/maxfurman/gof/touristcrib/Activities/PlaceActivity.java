package my.maxfurman.gof.touristcrib.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import my.maxfurman.gof.touristcrib.Fragments.PlaceFragment;
import my.maxfurman.gof.touristcrib.Model.MyPlace;
import my.maxfurman.gof.touristcrib.Utils.SingleFragmentActivity;
import my.maxfurman.gof.touristcrib.Utils.Constants;

/**
 * Created by maxfurman on 4/8/17.
 */

public class PlaceActivity extends SingleFragmentActivity {


    public static Intent newIntent(Context packageContext, MyPlace myPlace) {
        Intent intent = new Intent(packageContext, PlaceActivity.class);
        intent.putExtra(Constants.listItemResult, myPlace);
        return intent;
    }

    @Override
    protected Fragment createFragment() {
        MyPlace myPlace = getIntent().getParcelableExtra(Constants.listItemResult);
        return PlaceFragment.newInstance(myPlace);
    }



}
