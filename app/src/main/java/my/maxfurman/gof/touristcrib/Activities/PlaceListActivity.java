package my.maxfurman.gof.touristcrib.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import my.maxfurman.gof.touristcrib.Fragments.PlaceListFragment;
import my.maxfurman.gof.touristcrib.Utils.SingleFragmentActivity;
import my.maxfurman.gof.touristcrib.Utils.Constants;

import java.util.ArrayList;

/**
 * Created by maxfurman on 4/8/17.
 */

public class PlaceListActivity extends SingleFragmentActivity implements FragmentManager.OnBackStackChangedListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Listen for changes in the back stack
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        //Handle when activity is recreated like on orientation Change
        shouldDisplayHomeUp();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
    }

    @Override
    protected Fragment createFragment() {
        ArrayList<Integer> integerArrayListExtra = getIntent().getIntegerArrayListExtra(Constants.filterResult);
        return PlaceListFragment.newInstance(integerArrayListExtra);
    }

    public static Intent newIntent(Context packageContext, ArrayList<Integer> arrayList) {
        Intent intent = new Intent(packageContext, PlaceActivity.class);
        intent.putIntegerArrayListExtra(Constants.filterResult, arrayList);
        return intent;
    }

    @Override
    public void onBackStackChanged() {
        shouldDisplayHomeUp();
    }

    public void shouldDisplayHomeUp(){
        //Enable Up button only  if there are entries in the back stack
        boolean canback = getSupportFragmentManager().getBackStackEntryCount()>0;
        getSupportActionBar().setDisplayHomeAsUpEnabled(canback);
    }

    @Override
    public boolean onSupportNavigateUp() {
        //This method is called when the up button is pressed. Just the pop back stack.
        getSupportFragmentManager().popBackStack();
        return true;
    }

//    @Override
//    public void onBackPressed() {
//
//        int count = getFragmentManager().getBackStackEntryCount();
//
//        if (count == 0) {
//            super.onBackPressed();
//            //additional code
//        } else {
//            getFragmentManager().popBackStack();
//        }
//
//    }
}
