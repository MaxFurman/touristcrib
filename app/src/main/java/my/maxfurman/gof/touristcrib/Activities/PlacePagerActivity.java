package my.maxfurman.gof.touristcrib.Activities;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import my.maxfurman.gof.touristcrib.Fragments.PlaceFragment;
import my.maxfurman.gof.touristcrib.Model.MyPlace;
import my.maxfurman.gof.touristcrib.R;
import my.maxfurman.gof.touristcrib.Utils.PlaceSingleton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import my.maxfurman.gof.touristcrib.Utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by maxfurman on 4/10/17.
 */

public class PlacePagerActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {



    private ViewPager mViewPager;
    private List<MyPlace> mMyPlaces;

    private static Location mLastLocation;



    public static Intent newIntent(Context packageContext, MyPlace myPlace, ArrayList<Integer> filtersForViewPage, String query, Location lastLocation){
        Intent intent = new Intent(packageContext,PlacePagerActivity.class);
        intent.putExtra(Constants.listItemResult,myPlace);
        intent.putExtra(Constants.searchResult, query);
        intent.putIntegerArrayListExtra(Constants.filterResult,filtersForViewPage);
        mLastLocation = lastLocation;
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_pager);



        MyPlace myPlace = getIntent().getParcelableExtra(Constants.listItemResult);


        //PlaceSingleton placeSingleton = PlaceSingleton.get(getApplicationContext());
        mViewPager = (ViewPager) findViewById(R.id.activity_place_pager_view_pager);
        mMyPlaces = getSortedList();

        //mMyPlaces = placeSingleton.getPlaces();
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentPagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                MyPlace place = mMyPlaces.get(position);
                return new PlaceFragment().newInstance(place);
            }

            @Override
            public int getCount() {
                return mMyPlaces.size();
            }
        });




        for(int i = 0; i < mMyPlaces.size(); i++){
            if(mMyPlaces.get(i).getUuid().equals(myPlace.getUuid())){
                mViewPager.setCurrentItem(i);
                break;
            }
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public List<MyPlace> getSortedList(){

        PlaceSingleton placeSingleton = PlaceSingleton.get(getApplicationContext());
        ArrayList<MyPlace> listSingleton = placeSingleton.getPlaces();

        ArrayList<MyPlace> list = new ArrayList<>();
        ArrayList<Integer> arrayList = getIntent().getIntegerArrayListExtra(Constants.filterResult);
        for (MyPlace place : listSingleton) {
            if (place.getPriceIndex() == arrayList.get(0) || arrayList.get(0) == 0) {
                if (place.getDistrictIndex() == arrayList.get(1) || arrayList.get(1) == 0) {
                    if (place.getTypeIndex() == arrayList.get(2) || arrayList.get(2) == 0) {
                        list.add(place);
                    }
                }
            }
        }
        String query = getIntent().getStringExtra(Constants.searchResult);
        return filter(list,query);
    }

    private List<MyPlace> filter(List<MyPlace> places, String query) {
        final String lowerCaseQuery = query.toLowerCase();

//        Log.e("PLACE", "lowerCaseQuery = " + lowerCaseQuery);
//        for (MyPlace place : places) {
//            Log.e("PLACE", "place name = " + place.getPlaceName());
//        }
        final List<MyPlace> placeList = new ArrayList<>();

        for (MyPlace myPlace : places) {
            final String text = myPlace.getPlaceName().toLowerCase();
            if (text.contains(lowerCaseQuery)) {
                placeList.add(myPlace);
            }
        }

        if(mLastLocation == null){
            Collections.sort(placeList, new Comparator<MyPlace>() {
                public int compare(MyPlace o1, MyPlace o2) {
                    return Float.compare(o2.getPlaceRating(), o1.getPlaceRating());
                    //return o1.getPlaceRating().compareTo(o2.getPlaceRating());
                }
            });
        } else {
//            Collections.sort(placeList,
//                    new SortPlaces(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude())));
            for (MyPlace tempPlace: placeList)
            {
                Location tempLocation = new Location("");
                tempLocation.setLatitude(tempPlace.getLatitude());
                tempLocation.setLongitude(tempPlace.getLongitude());
                float distance = mLastLocation.distanceTo(tempLocation);
                distance = distance/1000;
                tempPlace.setPlaceDistance(distance);
            }

            Collections.sort(placeList, new Comparator<MyPlace>() {
                @Override
                public int compare(MyPlace c1, MyPlace c2) {
                    return new Float(c1.getPlaceDistance()).compareTo(new Float(c2.getPlaceDistance()));
                }
            });
        }

        return placeList;
    }


}
