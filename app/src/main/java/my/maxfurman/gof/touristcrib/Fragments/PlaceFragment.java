package my.maxfurman.gof.touristcrib.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import my.maxfurman.gof.touristcrib.Activities.MapActivity;
import my.maxfurman.gof.touristcrib.Model.MyPlace;
import my.maxfurman.gof.touristcrib.R;
import my.maxfurman.gof.touristcrib.Utils.CustomViewPagerAdapter;
import my.maxfurman.gof.touristcrib.Utils.DialogNet;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.appinvite.AppInvite;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import my.maxfurman.gof.touristcrib.Utils.Constants;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by maxfurman on 4/8/17.
 */

public class PlaceFragment extends Fragment implements ViewPager.OnPageChangeListener,
        View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    protected View view;
    private GoogleApiClient mGoogleApiClient;
    private Place place = null;
    private MyPlace myPlace;
    private ViewPager mViewPager;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private ImageView[] dots;
    private CustomViewPagerAdapter mAdapter;
    private RatingBar mRatingBar;
    private Button mMapButton;
    private Button mBackButton;

    private TextView mPlaceName;
    private TextView mPlaceAddress;
    private TextView mPlaceFilters;
    private TextView mPlaceDetails;

    private String myPlaceId;

    private AdView mAdView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);


        myPlace = getArguments().getParcelable(Constants.listItemResult);

        mGoogleApiClient = new GoogleApiClient
                .Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(LocationServices.API)
                .addApi(AppInvite.API)
                .build();





    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_place,null);

        mAdView = (AdView) v.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mPlaceName = (TextView) v.findViewById(R.id.place_name);
        mPlaceAddress = (TextView) v.findViewById(R.id.place_address);
        mPlaceFilters = (TextView) v.findViewById(R.id.place_filters);
        mPlaceDetails = (TextView) v.findViewById(R.id.place_details);
        mRatingBar = (RatingBar) v.findViewById(R.id.rating_Bar);
        mRatingBar.setEnabled(true);
        mRatingBar.setNumStars(5);

        myPlaceId = myPlace.getPlaceId();
        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient,myPlaceId);
        placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
            @Override
            public void onResult(@NonNull PlaceBuffer places) {
                if(places.getStatus().isSuccess() && places.getCount() > 0){
                    place = places.get(0);
                    if(myPlace != null){
                        //mPlaceName.setText(mPlace.getName());
                        mPlaceName.setText(place.getName());
                        mPlaceAddress.setText(place.getAddress());
                        mRatingBar.setRating(place.getRating());
                    }
                }
                places.release();
            }
        });


        String filters = getResources().getStringArray(R.array.filter_names)[0] +
                ": " + getResources().getStringArray(R.array.spinner_array1)[myPlace.getPriceIndex()]
                + "\n" + getResources().getStringArray(R.array.filter_names)[1] +
                ": " + getResources().getStringArray(R.array.spinner_array2)[myPlace.getDistrictIndex()]
                + "\n" + getResources().getStringArray(R.array.filter_names)[2] +
                ": " + getResources().getStringArray(R.array.spinner_array3)[myPlace.getTypeIndex()];

//        mPlaceName.setText(myPlace.getPlaceName());
//        mPlaceAddress.setText(myPlace.getPlaceAddress());
//        mPlaceFilters.setText(filters);
//        mPlaceDetails.setText(myPlace.getPlaceDetails());
//        mRatingBar.setRating(myPlace.getPlaceRating());



        mPlaceFilters.setText(filters);
        mPlaceDetails.setText(myPlace.getPlaceDetails());



        mViewPager = (ViewPager) v.findViewById(R.id.view_pager);
        pager_indicator = (LinearLayout) v.findViewById(R.id.viewPagerCountDots);
        mAdapter = new CustomViewPagerAdapter(getContext(),mGoogleApiClient,myPlace.getPlaceId());
        mViewPager.setAdapter(mAdapter);
        mViewPager.setCurrentItem(0);
        mViewPager.setOnPageChangeListener(this);



        mBackButton = (Button) v.findViewById(R.id.backButton);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(ItemActivity.this, ListActivity.class);
//                startActivityForResult(i,0);
//                finish();
                //onBackPressed();
                getFragmentManager().popBackStack();
                getActivity().onBackPressed();
            }
        });

        mMapButton = (Button) v.findViewById(R.id.mapButton);

        mMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isNetworkConnected()){
                    showNetworkDialog();
                    return;
                }
                Intent i = new Intent(getActivity(), MapActivity.class);
                i.putExtra(Constants.placeForMap,myPlace);
                startActivity(i);
            }
        });



        setUiPageViewController();

        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.item,menu);
        final MenuItem menuItem = menu.findItem(R.id.share);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                //onBackPressed();
                getFragmentManager().popBackStack();
                getActivity().onBackPressed();
                break;
            case R.id.share:
                onSharePressed();
                break;
            default:
                break;
        }

        return true;
    }

    public void onSharePressed(){
//        LatLng latLng = new LatLng(50.439871,30.52417299999999);
//        String coordinates = "geo:"+ latLng.latitude  +"," + latLng.longitude +"?q=" + myPlace.getPlaceName() + "," + myPlace.getPlaceAddress();
//        Log.e("coordinates", "" + coordinates);
//        Uri gmmIntentUri = Uri.parse(coordinates);
//        Log.e("coordinates", "" + gmmIntentUri);
//        Intent mapIntent = new Intent(Intent.ACTION_SEND, gmmIntentUri);
//        // Make the Intent explicit by setting the Google Maps package
//        mapIntent.setPackage("com.google.android.apps.maps");
//        mapIntent.putExtra(Intent.EXTRA_TEXT,coordinates);

        //https://www.google.com.ua/maps/place/Domino's+Pizza/@50.436695,30.5307936,15z





        String shareMessage = getShareMessage();

        //final String placeLink;
        String enc = "";
        try {
            enc = URLEncoder.encode(getShareMessage(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        final String placeLink = "Зацени какое место я нашёл через приложение TouristCrib! http://maps.google.com.ua/maps?q=" + enc;


//        URI uri = null;
//        try {
//            uri = new URI(
//                    "http",
//                    "maps.google.com.ua",
//                    "/maps?q=" + getShareMessage(),
//                    null);
//        } catch (URISyntaxException e) {
//            e.printStackTrace();
//        }
//        try {
//            URL url = uri.toURL();
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }

       // String request = uri.toASCIIString();



        Log.e("place", placeLink);


        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT,placeLink);
        startActivity(intent);

    }

    public String getShareMessage(){
        String placeNameAndAdress = myPlace.getPlaceName() + "+" + myPlace.getPlaceAddress();
        //Log.e("place", placeNameAndAdress);

        //String statement1 = placeNameAndAdress.replaceAll("\\s+","+");
        //String statement1 = placeNameAndAdress.replaceAll("\\s+","%20");
        //Log.e("place", placeNameAndAdress);
        //String statement1 = placeNameAndAdress.replaceAll(", ","+");

        String statement1 = placeNameAndAdress.replaceAll("\\s+","+");

        statement1 = statement1.replaceAll("'","");

        return statement1;
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//    }

    private void setUiPageViewController() {

        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getContext());
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
        }

        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public void showNetworkDialog(){
        DialogNet dialogNet = new DialogNet();
        dialogNet.show(getFragmentManager(),"dialogNet");
    }

    public static PlaceFragment newInstance(MyPlace myPlace){

        Bundle args = new Bundle();
        args.putParcelable(Constants.listItemResult, myPlace);

        PlaceFragment fragment = new PlaceFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }






}
