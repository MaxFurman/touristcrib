package my.maxfurman.gof.touristcrib.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RatingBar;
import android.widget.TextView;


import my.maxfurman.gof.touristcrib.Activities.PlacePagerActivity;
import my.maxfurman.gof.touristcrib.Model.MyPlace;
import my.maxfurman.gof.touristcrib.R;
import my.maxfurman.gof.touristcrib.Utils.DialogFilter;
import my.maxfurman.gof.touristcrib.Utils.DialogNet;
import my.maxfurman.gof.touristcrib.Utils.DialogProgress;
import my.maxfurman.gof.touristcrib.Utils.PlaceSingleton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.PlacePhotoResult;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import my.maxfurman.gof.touristcrib.Utils.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by maxfurman on 4/8/17.
 */

public class PlaceListFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, SearchView.OnQueryTextListener {


    private View view;
    private LinearLayout bottomLoadLayout;
    private LinearLayoutManager mLayoutManager;
    private GoogleApiClient mGoogleApiClient;
    private RecyclerView mRecyclerView;
    private PlaceAdapter mAdapter;
    private List<MyPlace> mPlaces;
    private ArrayList<Integer> mFilterArrayList;
    private List<MyPlace> mFilteredPlacesList;
    private ArrayList<String> placeSearchNames;
    private Bitmap mBitmap = null;
    private String searchQuery;
    private Location mLastLocation;

    private DialogProgress dialogProgress;


    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFilterArrayList = getArguments().getIntegerArrayList(Constants.filterResult);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(my.maxfurman.gof.touristcrib.R.layout.fragment_place_list, null);
        setHasOptionsMenu(true);
//        init();
//        getFirebaseData();
//        implementScrollListener();






        mGoogleApiClient = new GoogleApiClient
                .Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }






        searchQuery = "";



        //Bar
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("");

        return view;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void updateUI(ArrayList<MyPlace> arrayList) {
        //PlaceSingleton placeSingleton = PlaceSingleton.get(getApplicationContext());
        //List<MyPlace> places = placeSingleton.getPlaces();

        mFilteredPlacesList = new ArrayList<>();


        for (final MyPlace myPlace : arrayList) {
            if (myPlace.getPriceIndex() == mFilterArrayList.get(0) || mFilterArrayList.get(0) == 0) {
                if (myPlace.getDistrictIndex() == mFilterArrayList.get(1) || mFilterArrayList.get(1) == 0) {
                    if (myPlace.getTypeIndex() == mFilterArrayList.get(2) || mFilterArrayList.get(2) == 0) {
                        String myPlaceId = myPlace.getPlaceId();
                        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, myPlaceId);
                        placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                            @Override
                            public void onResult(@NonNull PlaceBuffer places) {
                                if (places.getStatus().isSuccess() && places.getCount() > 0) {
                                    Place place = places.get(0);
                                    if (place != null) {
                                        myPlace.setPlaceName(place.getName().toString());
                                    }
                                }
                                places.release();
                            }
                        });
                        mFilteredPlacesList.add(myPlace);
                    }
                }
            }
        }

        if(mLastLocation == null){
            Collections.sort(mFilteredPlacesList, new Comparator<MyPlace>() {
                public int compare(MyPlace o1, MyPlace o2) {
                    return Float.compare(o2.getPlaceRating(), o1.getPlaceRating());
                    //return o1.getPlaceRating().compareTo(o2.getPlaceRating());
                }
            });
        } else {
//            Collections.sort(mFilteredPlacesList,
//                    new SortPlaces(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude())));
//            for (final MyPlace myPlace : mFilteredPlacesList) {
//               // Log.e("PLACE", "" + myPlace.getPlaceName());
//            }

//            LatLng myHouse = new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude()) ;
//            Comparable comp = new Comparable () {
//                LatLng a;
//                public int compareTo(Object b) {
//                    int aDist = calcDistance(a, myHouse) ;
//                    int bDist = calcDistance(b, myHouse) ;
//                    return aDist - bDist;
//                }
//            };
//            myLonLatList.sort(lonLatList, comp);

            for (MyPlace tempPlace: mFilteredPlacesList)
            {
                Location tempLocation = new Location("");
                tempLocation.setLatitude(tempPlace.getLatitude());
                tempLocation.setLongitude(tempPlace.getLongitude());
                float distance = mLastLocation.distanceTo(tempLocation);
                distance = distance/1000;
                tempPlace.setPlaceDistance(distance);
            }

            Collections.sort(mFilteredPlacesList, new Comparator<MyPlace>() {
                @Override
                public int compare(MyPlace c1, MyPlace c2) {
                    return new Float(c1.getPlaceDistance()).compareTo(new Float(c2.getPlaceDistance()));
                }
            });
        }


//        ArrayList<MyPlace> list = new ArrayList<>();
//        list.addAll(mFilteredPlacesList);
        placeSearchNames = new ArrayList<>();
        mAdapter = new PlaceAdapter(getContext(),mFilteredPlacesList);
        mAdapter.setHasStableIds(true);
        //mAdapter.replaceAll(mFilteredPlacesList);
        //mAdapter.add(mFilteredPlacesList);
        mRecyclerView.scrollToPosition(0);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();



//        if (mAdapter == null) {
//            mAdapter = new PlaceAdapter(mFilteredPlacesList);
//            mAdapter.replaceAll(mFilteredPlacesList);
//            mRecyclerView.setAdapter(mAdapter);
//        } else {
//            mAdapter.notifyDataSetChanged();
//        }
    }


    private class PlaceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mTitleTextView;
        public ImageView mImageView;
        public TextView mPlaceFilters;
        public RatingBar mRatingBar;
        public TextView mDistanceTextView;


        public PlaceHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTitleTextView = (TextView) itemView.findViewById(R.id.place_title);
            mImageView = (ImageView) itemView.findViewById(R.id.img_list_item);
            mPlaceFilters = (TextView) itemView.findViewById(R.id.place_filters);
            mRatingBar = (RatingBar) itemView.findViewById(R.id.ratingBar_small);
            mRatingBar.setEnabled(true);
            mRatingBar.setNumStars(5);
            mDistanceTextView = (TextView) itemView.findViewById(R.id.distance);

//            mPriceTextView = (TextView) itemView.findViewById(R.id.place_price);
//            mDistrictTextView = (TextView) itemView.findViewById(R.id.place_district);
//            mTypeTextView = (TextView) itemView.findViewById(R.id.place_type);
        }


        @Override
        public void onClick(View v) {
            //if(!checkNetworkConnection())return;
            if (!isNetworkConnected()) {
                showNetworkDialog();
                return;
            }

            MyPlace clickedPlace = mPlaces.get(getAdapterPosition());
            //Intent i = PlaceActivity.newIntent(getActivity(), clickedPlace);
//            ArrayList<Integer> filtersForViewPage = new ArrayList<>();
//            filtersForViewPage.add(mFilterArrayList.get(0));
//            filtersForViewPage.add(mFilterArrayList.get(1));
//            filtersForViewPage.add(mFilterArrayList.get(2));
            Intent i = PlacePagerActivity.newIntent(getActivity(), clickedPlace, mFilterArrayList, searchQuery, mLastLocation);
            //i.putExtra(listItemResult,clickedPlace);

            startActivity(i);

//Open google maps
//            try {
//                    Create a Uri from an intent string. Use the result to create an Intent.
//                    String coordinates = "geo:" + latLng.latitude + "," +latLng.longitude + "?z=10&q=mafia печерский";
//                    Log.e(coordinates, ""+coordinates);
//                    Uri gmmIntentUri = Uri.parse(coordinates);
//                    // Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
//                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
//                    // Make the Intent explicit by setting the Google Maps package
//                    mapIntent.setPackage("com.google.android.apps.maps");
//                    // Attempt to start an activity that can handle the Intent
//                    if (mapIntent.resolveActivity(getPackageManager()) != null) {
//                        startActivity(mapIntent);
//                    }

//            } catch (GooglePlayServicesRepairableException e) {
//                e.printStackTrace();
//            } catch (GooglePlayServicesNotAvailableException e) {
//                e.printStackTrace();
//            }
        }


    }


    private class PlaceAdapter extends RecyclerView.Adapter<PlaceHolder> {

        private Context mContext;



        public PlaceAdapter(Context context, List<MyPlace> places) {
            this.mContext = context;

            mPlaces = new ArrayList<>();
            for(int i = 0; i < 10; i++){
                if (i >= places.size()) break;
                mPlaces.add(places.get(i));
            }
        }

        @Override
        public PlaceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater
                    .inflate(R.layout.list_item, parent, false);
            return new PlaceHolder(view);
        }

        @Override
        public void onBindViewHolder(final PlaceHolder holder, final int position) {


            final MyPlace myPlace = mPlaces.get(position);

            if(mLastLocation != null){
                Location location = new Location("A");
                location.setLatitude(myPlace.getLatitude());
                location.setLongitude(myPlace.getLongitude());
                double distance = location.distanceTo(mLastLocation);
                String distanceText = "-";
                if(distance < 1000){
                    distance = distance * 100;
                    int a = (int) Math.round(distance);
                    distance = (double) a/100;
                    distanceText = distance + "m";
                } else {
                    distance = distance / 10;
                    int a = (int) Math.round(distance);
                    distance = (double) a/100;
                    distanceText = distance + "km";
                }
                holder.mDistanceTextView.setText(distanceText);
            }


            String myPlaceId = myPlace.getPlaceId();
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, myPlaceId);
            placeResult.setResultCallback(new ResultCallback<PlaceBuffer>() {
                @Override
                public void onResult(@NonNull PlaceBuffer places) {
                    if (places.getStatus().isSuccess() && places.getCount() > 0) {
                        Place place = places.get(0);
                        if (place != null) {

                            String filters = getResources().getStringArray(R.array.filter_names)[0] +
                                    ": " + getResources().getStringArray(R.array.spinner_array1)[myPlace.getPriceIndex()]
                                    + "\n" + getResources().getStringArray(R.array.filter_names)[1] +
                                    ": " + getResources().getStringArray(R.array.spinner_array2)[myPlace.getDistrictIndex()]
                                    + "\n" + getResources().getStringArray(R.array.filter_names)[2] +
                                    ": " + getResources().getStringArray(R.array.spinner_array3)[myPlace.getTypeIndex()];

                            holder.mPlaceFilters.setText(filters);
                            holder.mTitleTextView.setText(place.getName());
                            holder.mRatingBar.setRating(place.getRating());
                        }
                    }
                    places.release();
                }
            });

            getPhotoForPlace(holder, myPlaceId);


            // Picasso.with(getApplicationContext()).load(mPlace.getAttributions()).resize(100,100).centerCrop().into(holder.mImageView);
        }

        @Override
        public int getItemCount() {
            return (null != mPlaces ? mPlaces.size() : 0);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public void filter(String text) {
            mPlaces.clear();
            if (text.isEmpty()) {
                mPlaces.addAll(mFilteredPlacesList);
            } else {
                text = text.toLowerCase();
                for (MyPlace myPlace : mFilteredPlacesList) {
                    if (myPlace.getPlaceName().toLowerCase().contains(text)) {
                        mPlaces.add(myPlace);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
//        Intent myIntent = new Intent(ListActivity.this, MainActivity.class);
//        startActivityForResult(myIntent, 0);
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                //onBackPressed();
                getFragmentManager().popBackStack();
                getActivity().onBackPressed();
                break;
            case R.id.action_filter:
                showFilterDialog();
                break;
            default:
                break;
        }


        return true;
    }

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        mAdapter.filter(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String query) {


//        final List<MyPlace> filteredPlaceList = filter(mPlaces, query);
//        searchQuery = query;
//
//        mAdapter.replaceAll(filteredPlaceList);
        //mBinding.recyclerView.scrollToPosition(0);
        searchQuery = query;
        mAdapter.filter(query);
        return true;
    }

    private List<MyPlace> filter(List<MyPlace> places, String query) {
        final String lowerCaseQuery = query.toLowerCase();
//        Log.e("PLACE", "lowerCaseQuery = " + lowerCaseQuery);
//        for (MyPlace place : places) {
//            Log.e("PLACE", "place name = " + place.getPlaceName());
//        }
        final List<MyPlace> placeList = new ArrayList<>();

        for (MyPlace myPlace : places) {
            final String text = myPlace.getPlaceName().toLowerCase();
            if (text.contains(lowerCaseQuery)) {
                placeList.add(myPlace);
            }
        }
//        Log.e("PLACE", "placeList size " + placeList.size());
        return placeList;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mFilterArrayList != null) {
            outState.putIntegerArrayList(Constants.filterResult, mFilterArrayList);
        }

        super.onSaveInstanceState(outState);
    }



    public void getFirebaseData() {


        showProgressDialog();

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference("places");
        mDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //HashMap<String,String> hashMap = new HashMap<String, String>();
                PlaceSingleton placeSingleton = PlaceSingleton.get(getActivity());
                //findViewById(R.id.loadingPanel).setVisibility(View.VISIBLE);

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    MyPlace place = postSnapshot.getValue(MyPlace.class);
                    place.setUuid(postSnapshot.getKey());
                    //Log.e("PLACE", "lat = " + place.getLatitude() + " , lon = " + place.getLongitude());

                    boolean alreadyIs = false;
                    for (MyPlace myPlace : placeSingleton.getPlaces()) {


                        if (place.getPlaceId().equals(myPlace.getPlaceId())) {
                            alreadyIs = true;
                            if (!place.equals(myPlace)) {
                                //Log.e("DATA", "place updated!");
                                placeSingleton.updatePlace(place);
                            }
                            break;
                        }
                    }


                    if (!alreadyIs) {
                        placeSingleton.addPlace(place);
                    }


                    //mDatabaseReference.removeEventListener(this);

                    //localList.add(myPlace);
                }

                //findViewById(R.id.loadingPanel).setVisibility(View.GONE);
                dialogProgress.dismissDialog();
//                Log.e("PLACE", "lat = " + placeSingleton.getPlaces().get(0).getLatitude() + " , lon = " + placeSingleton.getPlaces().get(0).getLongitude());
                updateUI(placeSingleton.getPlaces());


//                uploadDatabase(hashMap);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("DATA", "The read failed: " + databaseError.getMessage());
                //findViewById(R.id.loadingPanel).setVisibility(View.GONE);

            }
        });
    }


    private boolean isNetworkConnected() {
        try {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            return (mNetworkInfo == null) ? false : true;

        } catch (NullPointerException e) {
            return false;

        }

    }

    public boolean checkNetworkConnection() {
        if (!isNetworkConnected()) {
            LayoutInflater layoutInflater = (LayoutInflater) getContext()
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            View popupView = layoutInflater.inflate(R.layout.dialog_network, null);
            final PopupWindow popupWindow = new PopupWindow(popupView,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    true);

            Button btnDismiss = (Button) popupView.findViewById(R.id.dismiss);
            btnDismiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    popupWindow.dismiss();
                }
            });
            //popupWindow.showAsDropDown(mButton,mButton.getWidth()/2,-mButton.getHeight());
            popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
            return false;
        }
        return true;
    }

    public void showNetworkDialog() {
        DialogNet dialogNet = new DialogNet();
        dialogNet.show(getFragmentManager(), "dialogNet");
    }

    public void showFilterDialog() {
        DialogFilter dialogFilter = new DialogFilter(this, mFilterArrayList.get(0),
                mFilterArrayList.get(1), mFilterArrayList.get(2));
        dialogFilter.show(getFragmentManager(), "dialogFilter");
    }

    public void showProgressDialog() {
//        final DialogProgress dialogProgress = new DialogProgress();
//        Thread progressThread = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                dialogProgress.show(getFragmentManager(), "progressBar");
//            }
//        });
//        progressThread.start();

        dialogProgress = new DialogProgress();
        dialogProgress.show(getFragmentManager(), "progressBar");
    }

    public void updateFilter(ArrayList<Integer> arrayList) {
        mFilterArrayList = arrayList;
        PlaceSingleton placeSingleton = PlaceSingleton.get(getContext());
        updateUI(placeSingleton.getPlaces());
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        PlaceSingleton placeSingleton = PlaceSingleton.get(getContext());
//        updateUI(placeSingleton.getPlaces());
//    }

    public static PlaceListFragment newInstance(ArrayList<Integer> arrayList) {
        Bundle args = new Bundle();
        args.putIntegerArrayList(Constants.filterResult, arrayList);

        PlaceListFragment fragment = new PlaceListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
//        if (mGoogleApiClient != null) {
//            mGoogleApiClient.connect();
//        }
    }

    @Override
    public void onStop() {
//        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//            mGoogleApiClient.disconnect();
//        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onDestroy();
    }

    public void getPhotoForPlace(final PlaceHolder holder, final String placeId) {
        Places.GeoDataApi.getPlacePhotos(mGoogleApiClient, placeId)
                .setResultCallback(new ResultCallback<PlacePhotoMetadataResult>() {

                    @Override
                    public void onResult(PlacePhotoMetadataResult photos) {
                        if (!photos.getStatus().isSuccess()) {
                            return;
                        }

                        PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();
                        if (photoMetadataBuffer.getCount() > 0) {
                            // Display the first bitmap in an ImageView in the size of the view
                            photoMetadataBuffer.get(0)
                                    .getScaledPhoto(mGoogleApiClient, 200, 200)
                                    .setResultCallback(mDisplayPhotoResultCallback);
                        }
                        photoMetadataBuffer.release();
                    }

                    private ResultCallback<PlacePhotoResult> mDisplayPhotoResultCallback
                            = new ResultCallback<PlacePhotoResult>() {
                        @Override
                        public void onResult(PlacePhotoResult placePhotoResult) {
//                                if (!placePhotoResult.getStatus().isSuccess()) {
//                                    return;
//                                }
//                                mBitmap = placePhotoResult.getBitmap();
//                                holder.mImageView.setImageBitmap(mBitmap);

                            mBitmap = placePhotoResult.getBitmap();
                            holder.mImageView.setImageBitmap(mBitmap);

//                            if (!placePhotoResult.getStatus().isSuccess()) {
//                                Log.e("PHOTO", "ResultCallback is not success!");
//                                getPhotoForPlace(holder,placeId);
//                            }

                        }
                    };
                });
    }


    // Initialize the view
    private void init() {

        bottomLoadLayout = (LinearLayout) view.findViewById(R.id.loadItemsLayout_recyclerView);


        mRecyclerView = (RecyclerView) view.findViewById(R.id.place_recycler_view);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);

    }



    private boolean userScrolled = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    // Implement scroll listener
    private void implementScrollListener() {
        if(mRecyclerView == null) return;
        mRecyclerView
                .addOnScrollListener(new RecyclerView.OnScrollListener() {

                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView,
                                                     int newState) {

                        super.onScrollStateChanged(recyclerView, newState);

                        // If scroll state is touch scroll then set userScrolled
                        // true
                        if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                            userScrolled = true;

                        }

                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx,
                                           int dy) {

                        super.onScrolled(recyclerView, dx, dy);
                        // Here get the child count, item count and visibleitems
                        // from layout manager

                        visibleItemCount = mLayoutManager.getChildCount();
                        totalItemCount = mLayoutManager.getItemCount();
                        pastVisiblesItems = mLayoutManager
                                .findFirstVisibleItemPosition();

                        // Now check if userScrolled is true and also check if
                        // the item is end then update recycler view and set
                        // userScrolled to false
                        if (userScrolled
                                && (visibleItemCount + pastVisiblesItems) == totalItemCount) {
                            userScrolled = false;

                            updateRecyclerView();
                        }

                    }

                });

    }

    // Method for repopulating recycler view
    private void updateRecyclerView() {

        if(mPlaces.size() >= mFilteredPlacesList.size() || mPlaces.size() < 10) return;

        // Show Progress Layout
        bottomLoadLayout.setVisibility(View.VISIBLE);

        // Handler to show refresh for a period of time you can use async task
        // while commnunicating serve

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                // Loop for 5 items
                Log.e("PLACE", "size = " + mPlaces.size());
                for (int i = mPlaces.size(); i < mPlaces.size() + 5; i++) {
                    //int value = new RandomNumberGenerator().RandomGenerator();// Random
                    // value
                    if(i >= mFilteredPlacesList.size()) break;
                    // add random data to arraylist
                    mPlaces.add(mFilteredPlacesList.get(i));

                }
                mAdapter.notifyDataSetChanged();// notify adapter

                // Toast for task completion
//                Toast.makeText(getActivity(), "Items Updated.",
//                        Toast.LENGTH_SHORT).show();

                // After adding new data hide the view.
                bottomLoadLayout.setVisibility(View.GONE);

            }
        }, 3000);
    }

    public void getCurrentLocation(){

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getCurrentLocation();
        init();
        getFirebaseData();
        implementScrollListener();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }





}


