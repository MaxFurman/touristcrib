package my.maxfurman.gof.touristcrib.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by maxfurman on 2/11/17.
 */

public class MyPlace implements Parcelable {

    private String uuid;

    private String placeName;
    private String placeId;
    private String placeAddress;
    private String placeDetails;

    private float placeRating;

    private int mPriceIndex;
    private int mDistrictIndex;
    private int mTypeIndex;

    private double latitude;
    private double longitude;

    private float placeDistance;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }


    public String getPlaceId() {
        return placeId;
    }

    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    public String getPlaceAddress() {
        return placeAddress;
    }

    public void setPlaceAddress(String placeAddress) {
        this.placeAddress = placeAddress;
    }

    public String getPlaceDetails() {
        return placeDetails;
    }

    public void setPlaceDetails(String placeDetails) {
        this.placeDetails = placeDetails;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public int getPriceIndex() {
        return mPriceIndex;
    }

    public void setPriceIndex(int priceIndex) {
        mPriceIndex = priceIndex;
    }

    public int getDistrictIndex() {
        return mDistrictIndex;
    }

    public void setDistrictIndex(int districtIndex) {
        this.mDistrictIndex = districtIndex;
    }

    public int getTypeIndex() {
        return mTypeIndex;
    }

    public void setTypeIndex(int typeIndex) {
        this.mTypeIndex = typeIndex;
    }

    public float getPlaceRating() {
        return placeRating;
    }

    public void setPlaceRating(float placeRating) {
        this.placeRating = placeRating;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public float getPlaceDistance() {
        return placeDistance;
    }

    public void setPlaceDistance(float placeDistance) {
        this.placeDistance = placeDistance;
    }

    public MyPlace(String uuid, String placeId, String placeName, String placeAddress, String placeDetails,
                   double latitude, double longitude,
                   float placeRating, int priceIndex, int districtIndex, int typeIndex) {
        this.uuid = uuid;
        this.placeId = placeId;
        this.mPriceIndex = priceIndex;
        this.mDistrictIndex = districtIndex;
        this.mTypeIndex = typeIndex;
        this.placeName = placeName+"";
        this.placeDetails = placeDetails+"";
        this.placeAddress = placeAddress+"";
        this.placeRating = placeRating;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public MyPlace(Parcel in) {
        String[] stringsData = new String[5];
        in.readStringArray(stringsData);
        uuid = stringsData[0];
        placeId = stringsData[1];
        placeName = stringsData[2];
        placeAddress = stringsData[3];
        placeDetails = stringsData[4];

        int[] intData = new int[3];
        in.readIntArray(intData);
        mPriceIndex = intData[0];
        mDistrictIndex = intData[1];
        mTypeIndex = intData[2];

        float rate = in.readFloat();
        placeRating = rate;


    }

    public MyPlace(){
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{uuid, placeId,placeName,placeAddress,placeDetails});
        dest.writeIntArray(new int[]{mPriceIndex,mDistrictIndex,mTypeIndex});
        dest.writeFloat(placeRating);
    }

    public static final Creator<MyPlace> CREATOR = new Creator<MyPlace>() {

        @Override
        public MyPlace createFromParcel(Parcel source) {
            return new MyPlace(source);
        }

        @Override
        public MyPlace[] newArray(int size) {
            return new MyPlace[size];
        }
    };
}
