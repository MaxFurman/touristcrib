package my.maxfurman.gof.touristcrib.Utils;

/**
 * Created by maxfurman on 2/12/17.
 */

public class Constants {
    public static final String EXTRA = "com.example.gof.gofproject.MyPlace";
    public static final int NUM_ITEMS = 6;
    public static final String filterResult = "filterResult";
    public static final String listItemResult = "listItemResult";
    public static final String pagerItem = "listItem";
    public static final String placeForMap = "placeForMap";
    public static final String mapResult = "mapResult";
    public static final String searchResult = "searchResult";

    public static final String list = "LIST";

    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
}
