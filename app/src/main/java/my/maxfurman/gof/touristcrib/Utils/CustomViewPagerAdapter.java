package my.maxfurman.gof.touristcrib.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import my.maxfurman.gof.touristcrib.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.PlacePhotoResult;
import com.google.android.gms.location.places.Places;

import java.util.ArrayList;

/**
 * Created by maxfurman on 2/17/17.
 */

public class CustomViewPagerAdapter extends PagerAdapter implements GoogleApiClient.OnConnectionFailedListener {


    private Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private String placeId;

    //    private LayoutInflater inflater;
//    private ViewGroup layout;
//    private ImageView imageView;
    private ArrayList<Bitmap> placeBitmaps;

    public CustomViewPagerAdapter(Context context, GoogleApiClient mGoogleApiClient, String placeId) {
        this.mGoogleApiClient = mGoogleApiClient;
        this.placeId = placeId;
        mContext = context;
        placeBitmaps = new ArrayList<>();

    }

    @Override
    public Object instantiateItem(final ViewGroup collection, final int position) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        final ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.view_pager, collection, false);


        Places.GeoDataApi.getPlacePhotos(mGoogleApiClient, placeId)
                .setResultCallback(new ResultCallback<PlacePhotoMetadataResult>() {

                    @Override
                    public void onResult(PlacePhotoMetadataResult photos) {
                        if (!photos.getStatus().isSuccess()) {
                            return;
                        }

                        PlacePhotoMetadataBuffer photoMetadataBuffer = photos.getPhotoMetadata();
                        if (photoMetadataBuffer.getCount() > 0) {
                            // Display the bitmap in an ImageView in the size of the view
                            if (photoMetadataBuffer.getCount() > position) {
                                photoMetadataBuffer.get(position)
                                        .getScaledPhoto(mGoogleApiClient, 200, 200)
                                        .setResultCallback(mDisplayPhotoResultCallback);

                            } else {
                                ImageView imageView = (ImageView) layout.findViewById(R.id.img_page_view);
                                imageView.setImageResource(R.drawable.no_photo);
                            }
                        }
                        photoMetadataBuffer.release();
                    }

                    private ResultCallback<PlacePhotoResult> mDisplayPhotoResultCallback
                            = new ResultCallback<PlacePhotoResult>() {
                        @Override
                        public void onResult(PlacePhotoResult placePhotoResult) {
                            if (!placePhotoResult.getStatus().isSuccess()) {
                                return;
                            }

                            ImageView imageView = (ImageView) layout.findViewById(R.id.img_page_view);
                            Bitmap bitmap = placePhotoResult.getBitmap();
                            if (imageView.getDrawable() == null){
                                imageView.setImageBitmap(bitmap);
                            } 


                        }
                    };
                });


        collection.addView(layout);


        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        //collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


//        @Override
//        public CharSequence getPageTitle(int position) {
//            CustomViewPagerEnum customPagerEnum = CustomViewPagerEnum.values()[position];
//            return mContext.getString(customPagerEnum.getPictureId());
//        }
}
