package my.maxfurman.gof.touristcrib.Utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import my.maxfurman.gof.touristcrib.Fragments.PlaceListFragment;
import my.maxfurman.gof.touristcrib.R;

import java.util.ArrayList;

/**
 * Created by maxfurman on 4/5/17.
 */

public class DialogFilter extends DialogFragment implements View.OnClickListener {

    private Fragment mFragment;

    private Spinner mSpinnerPrice;
    private Spinner mSpinnerDistrict;
    private Spinner mSpinnerType;

    private Button mButton;

    private int mSpinnerPriceIndex;
    private int mSpinnerDistrictIndex;
    private int mSpinnerTypeIndex;

    public DialogFilter() {}

    public DialogFilter(PlaceListFragment fragment, int priceIndex, int districtIndex, int typeIndex){
        mFragment = fragment;
        mSpinnerPriceIndex = priceIndex;
        mSpinnerDistrictIndex = districtIndex;
        mSpinnerTypeIndex = typeIndex;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_filter,null);


        initSpinner(v,mSpinnerPrice, R.id.spinner1, R.array.spinner_array1,mSpinnerPriceIndex);
        initSpinner(v,mSpinnerDistrict, R.id.spinner2, R.array.spinner_array2,mSpinnerDistrictIndex);
        initSpinner(v,mSpinnerType, R.id.spinner3, R.array.spinner_array3,mSpinnerTypeIndex);

        mButton = (Button) v.findViewById(R.id.button);
        mButton.setOnClickListener(this);

        return v;
    }



    @Override
    public void onClick(View v) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(mSpinnerPriceIndex);
        arrayList.add(mSpinnerDistrictIndex);
        arrayList.add(mSpinnerTypeIndex);

//        ListActivity callingActivity = (ListActivity) getActivity();
//        callingActivity.updateFilter(arrayList);
        PlaceListFragment callingFragment = (PlaceListFragment) mFragment;
        callingFragment.updateFilter(arrayList);
        dismiss();
    }


    protected void initSpinner(View v, Spinner mSpinner, int spinId, int array, int selection){
        mSpinner = (Spinner) v.findViewById(spinId);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(v.getContext(),
                array, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.spinner_item);
        // Apply the adapter to the spinner
        mSpinner.setAdapter(adapter);

        mSpinner.setSelection(selection);

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (parent.getId()){
                    case R.id.spinner1:
                        mSpinnerPriceIndex = position;
                        break;
                    case R.id.spinner2:
                        mSpinnerDistrictIndex = position;
                        break;
                    case R.id.spinner3:
                        mSpinnerTypeIndex = position;
                        break;
                    default:
                        break;
                }

                //Toast.makeText(getApplicationContext(), parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }
}
