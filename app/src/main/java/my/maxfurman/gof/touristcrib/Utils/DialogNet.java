package my.maxfurman.gof.touristcrib.Utils;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * Created by maxfurman on 3/27/17.
 */

public class DialogNet extends DialogFragment implements View.OnClickListener {

    private Button okButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(my.maxfurman.gof.touristcrib.R.layout.dialog_network,null);

        okButton = (Button) v.findViewById(my.maxfurman.gof.touristcrib.R.id.dismiss);
        okButton.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }


}
