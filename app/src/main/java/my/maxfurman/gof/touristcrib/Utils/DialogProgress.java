package my.maxfurman.gof.touristcrib.Utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import my.maxfurman.gof.touristcrib.R;


/**
 * Created by maxfurman on 4/11/17.
 */

public class DialogProgress extends DialogFragment {

    ProgressBar mProgressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_progress,null);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        setCancelable(false);
        return view;
    }

    public void dismissDialog(){
        dismiss();
    }
}