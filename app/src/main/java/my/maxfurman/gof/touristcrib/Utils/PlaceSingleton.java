package my.maxfurman.gof.touristcrib.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import my.maxfurman.gof.touristcrib.Model.MyPlace;
import my.maxfurman.gof.touristcrib.database.PlaceBaseHelper;
import my.maxfurman.gof.touristcrib.database.PlaceCursorWrapper;

import java.util.ArrayList;
import java.util.UUID;

import my.maxfurman.gof.touristcrib.database.PlacesDatabase;

/**
 * Created by maxfurman on 2/12/17.
 */

public class PlaceSingleton {
    private static PlaceSingleton mPlaceSingleton;
    private Context mContext;
    private SQLiteDatabase mDatabase;
//    private List<MyPlace> mPlaces;




    public static PlaceSingleton get(Context context){
        if(mPlaceSingleton == null){
            mPlaceSingleton = new PlaceSingleton(context);
        }
        return  mPlaceSingleton;
    }

    private PlaceSingleton(Context context){

        mContext = context.getApplicationContext();
        mDatabase = new PlaceBaseHelper(mContext)
                .getWritableDatabase();


//        mPlaces = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            CustomViewPagerEnum customViewPagerEnum = CustomViewPagerEnum.values()[i];
//            int pictureId = customViewPagerEnum.getPictureId();
//
//            MyPlace place = new MyPlace();
//            place.setPriceIndex(i);
//            place.setDistrictIndex(i);
//            place.setTypeIndex(i);
//            place.setPlaceName(customViewPagerEnum.toString());
//            place.setPlaceId("ChIJI1_Rxf_O1EARi3XXdnblb5Q");
//            mPlaces.add(place);
//        }
    }


    public ArrayList<MyPlace> getPlaces() {
        ArrayList<MyPlace> myPlaces = new ArrayList<>();
        PlaceCursorWrapper cursor = queryPlaces(null,null);

        try{
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
                myPlaces.add(cursor.getMyPlace());
                cursor.moveToNext();
            }

        } finally {
            cursor.close();
        }

        return myPlaces;
    }

    public MyPlace getPlace(UUID id){
//        for(MyPlace place : mPlaces){
//            if(place.getPlaceId().equals(id)){
//                return place;
//            }
//        }
        PlaceCursorWrapper cursor = queryPlaces(
                PlacesDatabase.PlaceTable.UUID + " = ?",
                new String[] { id.toString() }
        );

        try{
            if(cursor.getCount() == 0){
                return null;
            }

            cursor.moveToFirst();
            return  cursor.getMyPlace();
        } finally {
            cursor.close();
        }
    }

    private static ContentValues getContentValues(MyPlace myPlace){
        ContentValues values = new ContentValues();
        values.put(PlacesDatabase.PlaceTable.UUID,myPlace.getUuid());
        values.put(PlacesDatabase.PlaceTable.PLACEID,myPlace.getPlaceId());
        values.put(PlacesDatabase.PlaceTable.NAME,myPlace.getPlaceName());
        values.put(PlacesDatabase.PlaceTable.ADDRESS,myPlace.getPlaceAddress());
        values.put(PlacesDatabase.PlaceTable.DETAILS,myPlace.getPlaceDetails());
        values.put(PlacesDatabase.PlaceTable.LATITUDE,myPlace.getLatitude());
        values.put(PlacesDatabase.PlaceTable.LONGITUDE,myPlace.getLongitude());
        values.put(PlacesDatabase.PlaceTable.RATING,myPlace.getPlaceRating());
        values.put(PlacesDatabase.PlaceTable.PRICE,myPlace.getPriceIndex());
        values.put(PlacesDatabase.PlaceTable.DISTRICT,myPlace.getDistrictIndex());
        values.put(PlacesDatabase.PlaceTable.TYPE,myPlace.getTypeIndex());

        return values;
    }

    public void addPlace(MyPlace myPlace){
        ContentValues values = getContentValues(myPlace);
        mDatabase.insert(PlacesDatabase.PlaceTable.NAME, null, values);
    }

    public void updatePlace(MyPlace myPlace){
        String uuid = myPlace.getUuid();
        ContentValues values = getContentValues(myPlace);

        mDatabase.update(PlacesDatabase.PlaceTable.NAME, values,
                PlacesDatabase.PlaceTable.UUID + " = ?",
                new String[]{ uuid });
    }

    private PlaceCursorWrapper queryPlaces(String whereClause, String[] whereArgs){
        Cursor cursor = mDatabase.query(
                PlacesDatabase.PlaceTable.NAME,
                null, // Columns - null выбирает все столбцы
                whereClause,
                whereArgs,
                null, // groupBy
                null, // having
                null // orderBy
        );
        return new PlaceCursorWrapper(cursor);
    }
}

