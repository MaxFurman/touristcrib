package my.maxfurman.gof.touristcrib.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by maxfurman on 3/18/17.
 */

public class PlaceBaseHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "placeBase.db";

    public PlaceBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + PlacesDatabase.PlaceTable.NAME + "(" +
                " _id integer primary key autoincrement, " +
                PlacesDatabase.PlaceTable.UUID + ", " +
                PlacesDatabase.PlaceTable.PLACEID + ", " +
                PlacesDatabase.PlaceTable.NAME + ", " +
                PlacesDatabase.PlaceTable.ADDRESS + ", " +
                PlacesDatabase.PlaceTable.DETAILS + ", " +
                PlacesDatabase.PlaceTable.LATITUDE + ", " +
                PlacesDatabase.PlaceTable.LONGITUDE + ", " +
                PlacesDatabase.PlaceTable.RATING + ", " +
                PlacesDatabase.PlaceTable.PRICE + ", " +
                PlacesDatabase.PlaceTable.DISTRICT + ", " +
                PlacesDatabase.PlaceTable.TYPE + ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
