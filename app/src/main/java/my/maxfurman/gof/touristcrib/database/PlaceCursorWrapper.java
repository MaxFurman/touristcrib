package my.maxfurman.gof.touristcrib.database;

import android.database.Cursor;
import android.database.CursorWrapper;

import my.maxfurman.gof.touristcrib.Model.MyPlace;

/**
 * Created by maxfurman on 3/18/17.
 */

public class PlaceCursorWrapper extends CursorWrapper {

    public PlaceCursorWrapper(Cursor cursor){
        super(cursor);
    }

    public MyPlace getMyPlace(){
        String uuid = getString(getColumnIndex(PlacesDatabase.PlaceTable.UUID));
        String placeid = getString(getColumnIndex(PlacesDatabase.PlaceTable.PLACEID));
        String name = getString(getColumnIndex(PlacesDatabase.PlaceTable.NAME));
        String address = getString(getColumnIndex(PlacesDatabase.PlaceTable.ADDRESS));
        String details = getString(getColumnIndex(PlacesDatabase.PlaceTable.DETAILS));
        double latitude = getDouble(getColumnIndex(PlacesDatabase.PlaceTable.LATITUDE));
        double longitude = getDouble(getColumnIndex(PlacesDatabase.PlaceTable.LONGITUDE));
        float rating = getFloat(getColumnIndex(PlacesDatabase.PlaceTable.RATING));
        int price = getInt(getColumnIndex(PlacesDatabase.PlaceTable.PRICE));
        int district = getInt(getColumnIndex(PlacesDatabase.PlaceTable.DISTRICT));
        int type = getInt(getColumnIndex(PlacesDatabase.PlaceTable.TYPE));

        MyPlace myPlace = new MyPlace(uuid,placeid,name,address,details,
                latitude,longitude,rating,price,district,type);

        return myPlace;
    }
}
