package my.maxfurman.gof.touristcrib.database;

/**
 * Created by maxfurman on 3/18/17.
 */

public class PlacesDatabase {

    public static final class PlaceTable{
        public static final String UUID = "uuid";
        public static final String PLACEID = "PLACEID";
        public static final String NAME = "name";
        public static final String ADDRESS = "adress";
        public static final String DETAILS = "details";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String RATING = "rating";
        public static final String PRICE = "price";
        public static final String DISTRICT = "district";
        public static final String TYPE = "type";

    }
}
